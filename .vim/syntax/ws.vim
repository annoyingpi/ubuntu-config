"set tab size to 1
setl ts=1
syn clear
syn match Space ' '
syn match Tab '\t'
hi Space term=NONE cterm=NONE ctermfg=NONE ctermbg=Blue
hi Tab term=NONE cterm=NONE ctermfg=NONE ctermbg=Red
