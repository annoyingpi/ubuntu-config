set tabstop=3
set shiftwidth=3
set textwidth=80 
syntax on
set ai
set lbr
set incsearch
set ignorecase
set pastetoggle=<F2>
nnoremap ; :

highlight ExtraWhitespace ctermbg=darkgreen guibg=darkgreen
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
" Show trailing whitespace:
"match ExtraWhitespace /\s\+$/
" Show tabs that are not at the start of a line:
" :match ExtraWhitespace /[^\t]\zs\t\+/
"nnoremap ,wn :match ExtraWhitespace /^\s* \s*\<Bar>\s\+$/<CR>
"nnoremap ,wf :match<CR>


colorscheme desert 
"colorscheme ir_black 

"gvim options
"if has("gui_macvim")
"    set guioptions-=T
"    set ruler
"    let macvim_hig_shift_movement = 1
"endif

"common typos
iab hte the
iab teh the
iab taht that
iab htat that
iab THe The
iab THen Then
iab THey They
iab THis This
iab THere There
iab THerefore Therefore
iab THose Those
iab COnsider Consider
iab SUppose Suppose
iab WHen When

iab repn representation
iab repns representations
iab subrepn subrepresentation
iab subrepns subrepresentations
iab holo holomorphic


"map - <C-W>-
"map + <C-W>+
map <F5> :set hls!<bar>set hls?<CR>

filetype plugin on
filetype indent on

"python shortcuts
au BufEnter *.py set textwidth=0
au BufEnter *.py set number
au BufEnter *.py map # :s/^/#/<CR>
au BufEnter *.py map ,# :s/^#//<CR>
au BufEnter *.py iab pr print

"LaTeX shortcuts
au BufEnter *.tex 
\set spell spelllang=en_us 
\|iab \( \left(
\|iab \) \right)
\|imap \[ \[ <CR><CR>\] <ESC>ki
\|imap \crl \left\{  \right\}<ESC>8hi
\|imap \ddx \frac{d}{dx}
\|imap \ddy \frac{d}{dy}
\|imap \ddz \frac{d}{dz}
\|imap \pdx \frac{\partial}{\partial x}
\|imap \pdy \frac{\partial}{\partial y}
\|imap \pdz \frac{\partial}{\partial z}
\|imap ^^ ^{}<ESC>i
\|imap __ _{}<ESC>i
\|imap `` ``''<ESC>hi
\|imap \textbf \textbf{}<ESC>i
\|imap \textit \textit{}<ESC>i
\|imap \textsc \textsc{}<ESC>i
\|imap \texttt \texttt{}<ESC>i
\|imap \emph \emph{}<ESC>i
\|imap COR \begin{corollary} <CR><CR>\end{corollary} <ESC>ki
\|imap CONJ \begin{conjecture} <CR><CR>\end{conjecture} <ESC>ki
\|imap DEF \begin{definition} <CR><CR>\end{definition} <ESC>ki
\|imap DOC \begin{document} <CR><CR>\end{document} <ESC>ki
\|imap EXAM \begin{example} <CR><CR>\end{example} <ESC>ki
\|imap EXER \begin{exercise} <CR><CR>\end{exercise} <ESC>ki
\|imap FRM \begin{frame}{} <CR><CR>\end{frame} <ESC>2k3li
\|imap LEM \begin{lemma} <CR><CR>\end{lemma} <ESC>ki
\|imap MOT \begin{motivation} <CR><CR>\end{motivation} <ESC>ki
\|imap JOKE \begin{joke} <CR><CR>\end{joke} <ESC>ki
\|imap ALGOR \begin{algorithm} <CR><CR>\end{algorithm} <ESC>ki
\|imap NOTE \begin{note} <CR><CR>\end{note} <ESC>ki
\|imap FACT \begin{fact} <CR><CR>\end{fact} <ESC>ki
\|imap PROP \begin{proposition} <CR><CR>\end{proposition} <ESC>ki
\|imap PROB \begin{problem} <CR><CR>\end{problem} <ESC>ki
\|imap PF \begin{proof} <CR><CR>\end{proof} <ESC>ki
\|imap QUES \begin{question} <CR><CR>\end{question} <ESC>ki
\|imap SOLN \begin{solution} <CR><CR>\end{solution} <ESC>ki
\|imap REM \begin{remark} <CR><CR>\end{remark} <ESC>ki
\|imap THM \begin{theorem} <CR><CR>\end{theorem} <ESC>ki
\|imap ALIGN \begin{align*} <CR><CR>\end{align*} <ESC>ki
\|imap EQN \begin{equation} <CR><CR>\end{equation} <ESC>ki
\|imap MAT \begin{matrix} <CR><CR>\end{matrix} <ESC>ki
\|imap PMAT \begin{pmatrix} <CR><CR>\end{pmatrix} <ESC>ki
\|imap BMAT \begin{bmatrix} <CR><CR>\end{bmatrix} <ESC>ki
\|imap TAB \begin{tabular}{} <CR><CR>\end{tabular} <ESC>2k3li
\|imap ENUM \begin{enumerate}<CR>\item <CR>\item <CR>\end{enumerate}<Up><Up>
\|imap ITEM \begin{itemize}<CR>\item <CR>\item <CR>\end{itemize}<Up><Up>
\|imap XYM \xymatrix{ <CR><CR>} <ESC>ki
\|imap CENTER \begin{center} <CR><CR>\end{center} <ESC>ki
\|imap TIKZ \begin{tikzpicture} <CR><CR>\end{tikzpicture} <ESC>ki


" Map <F6> to 'escape the current \begin .. \end environment
"imap <F6> /\\end{.*}/ei<Right>
imap <S-Tab>C diwi\mathcal{pi<Right>}
imap <S-Tab>B diwi\mathbb{pi<Right>}
imap <S-Tab>F diwi\mathfrak{pi<Right>}
imap <S-Tab>R diwi\mathrm{pi<Right>}
imap <S-Tab>O diwi\mathop{pi<Right>}
imap <S-Tab>= diWi\bar{pi<Right>}
imap <S-Tab>. diWi\dot{pi<Right>}
imap <S-Tab>" diWi\ddot{pi<Right>}
imap <S-Tab>- diWi\overline{pi<Right>}
imap <S-Tab>^ diWi\widehat{pi<Right>}
imap <S-Tab>~ diWi\widetilde{pi<Right>}
imap <S-Tab>_ diWi\underline{pi<Right>}


au BufNewFile,BufRead *.ws set filetype=ws
